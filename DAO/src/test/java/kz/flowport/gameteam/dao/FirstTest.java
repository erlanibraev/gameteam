package kz.flowport.gameteam.dao;

import kz.flowport.gameteam.dao.entities.TournamentEntity;
import kz.flowport.gameteam.dao.entities.UsersEntity;
import kz.flowport.gameteam.dao.repository.IUsersRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
/**
 * Created by Erlan Ibraev on 03.05.2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Slf4j
public class FirstTest {
    @Autowired
    private IUsersRepository usersRepository;

    @Test
    public void usersRepositoryFindAllTest() {
        List<UsersEntity> usersList = usersRepository.findAll();
        assertNotNull(usersList);
    }

    @Test
    public void addData() {
        UsersEntity entity = new UsersEntity();
        entity.setEmail(UUID.randomUUID().toString());
        entity.setNickname(UUID.randomUUID().toString());
        entity.setPassword(UUID.randomUUID().toString());
        entity.setCountry("KZ");
        UsersEntity result = usersRepository.save(entity);
        assertNotNull(result);
        log.info(String.valueOf(result.getId()));
        log.info(result.getNickname());
    }
}
