package kz.flowport.gameteam.dao.entities;

import kz.flowport.gameteam.dao.models.Games;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="squads")
@Setter @Getter
public class SquadsEntity extends AbstractEntity {

    @Enumerated(EnumType.ORDINAL)
    @Column(name="game_id")
    private Games gameId;

    @Column(name="max_slot")
    private Integer maxSlot = 8;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="team_id")
    private TeamsEntity team;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="squad_capitan")
    private UsersEntity squadCapitan;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "squad", cascade = CascadeType.ALL)
    private Set<SquadSlotEntity> slots;

    @Column(name="active")
    private boolean active = true;

}
