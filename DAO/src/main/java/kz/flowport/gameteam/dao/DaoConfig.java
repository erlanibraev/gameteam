package kz.flowport.gameteam.dao;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Configuration
@EnableJpaRepositories(basePackages = {"kz.flowport.gameteam.dao.repository"})
@EntityScan(value = {"kz.flowport.gameteam.dao.entities"})
@EnableTransactionManagement
public class DaoConfig {
}
