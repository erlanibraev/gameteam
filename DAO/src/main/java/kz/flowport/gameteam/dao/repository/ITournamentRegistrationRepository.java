package kz.flowport.gameteam.dao.repository;

import kz.flowport.gameteam.dao.entities.TournamentRegistrationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Repository
public interface ITournamentRegistrationRepository extends JpaRepository<TournamentRegistrationEntity, Long>{
}
