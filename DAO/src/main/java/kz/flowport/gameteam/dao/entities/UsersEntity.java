package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="users", uniqueConstraints = {@UniqueConstraint(columnNames = {"nickname"}), @UniqueConstraint(columnNames = {"email"})})
@Setter @Getter
public class UsersEntity extends AbstractEntity {

    @Column(name="email", length = 1000)
    private String email;

    @Column(name="nickname", length = 1000)
    private String nickname;

    @Column(name="password", length = 1000)
    private String password;

    @Column(name="country", length = 2)
    private String country;

    @Column(name="first_name", length = 1000)
    private String firstName;

    @Column(name="second_name", length = 1000)
    private String secondName;

    @Column(name="middle_name", length = 1000)
    private String middleName;

    @Column(name="birthday")
    private Date birthday;

    @Column(name="avatar")
    @Lob
    private byte[] avatar;

}
