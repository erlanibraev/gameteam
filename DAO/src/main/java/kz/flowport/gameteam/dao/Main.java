package kz.flowport.gameteam.dao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@SpringBootApplication
@ComponentScan({"kz.flowport.gameteam"})
public class Main {

    public static final void main(String... args) {
        SpringApplication.run(Main.class, args);
    }
}
