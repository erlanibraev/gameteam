package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="challenge_template")
@Getter @Setter
public class ChallengeEntity extends AbstractEntity {

    @Column(name="start_date")
    private Timestamp startDate;

    @Column(name="end_date")
    private Timestamp endDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="challenge_template_id")
    private ChallengeTemplateEntity challengeTemplate;
}
