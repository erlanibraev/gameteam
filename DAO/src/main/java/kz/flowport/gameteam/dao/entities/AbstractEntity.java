package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
    @Id
    @Setter @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Setter @Getter
    @Column(name="create_datetime")
    private Timestamp createDatetime;

    @Setter @Getter
    @Column(name="change_datetime")
    private Timestamp changeDatetime;

    @PostPersist
    protected void generateDatetime() {
        if (createDatetime == null) {
            createDatetime = Timestamp.valueOf(LocalDateTime.now());
        }
        changeDatetime = Timestamp.valueOf(LocalDateTime.now());
    }
}
