package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="news")
@Getter @Setter
public class NewsEntity extends AbstractEntity {

    @Column(name="news", length = 5000)
    private String news;

    @Column(name="title", length = 5000)
    private String title;

}
