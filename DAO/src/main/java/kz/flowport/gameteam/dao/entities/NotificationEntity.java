package kz.flowport.gameteam.dao.entities;

import kz.flowport.gameteam.dao.models.NotificationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="notofications")
@Setter @Getter
public class NotificationEntity extends AbstractEntity {

    @Column(name="notification_type")
    @Enumerated(EnumType.ORDINAL)
    private NotificationType notificationType;

    @Column(name="notification_message", length = 1000)
    private String notificationMessage;

    @Column(name="is_readed")
    private boolean isReaded;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="from")
    private UsersEntity from;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="to")
    private UsersEntity to;

}
