package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="round")
@Setter @Getter
public class RoundEntity extends AbstractEntity {

    @Column(name="round")
    public Integer round;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tournament_grid_id")
    private TournamentGridEntity tournamentGrid;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="team1")
    private TeamsEntity team1; // TODO: 03.05.2017 Уточнить

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="team2")
    private TeamsEntity team2; // TODO: 03.05.2017 Уточнить

    @Column(name="is_ended")
    private Boolean isEnded;

    @Column(name="score1")
    private Double score1;

    @Column(name="score2")
    private Double score2;

    @Column(name="round_time")
    private Timestamp roundTime;

    @Column(name="text_message", length = 1000)
    private String textMessage;

    @Column(name="manual_start")
    private Timestamp manualStart;

}
