package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="tournament")
@Setter @Getter
public class TournamentEntity extends AbstractEntity {

    @Column(name="reg_start_date")
    private Timestamp regStartDate;

    @Column(name="reg_finish_date")
    private Timestamp regFinishDate;

    @Column(name="tournament_start_date")
    private Timestamp tournamentStartDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="tournament_template_id")
    private TournamentTemplateEntity tournamentTemplate;

    @OneToMany(mappedBy = "tournament", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<PrizePlaceEntity> prizePlace;
}
