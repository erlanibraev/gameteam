package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name = "prize_place")
@Setter @Getter
public class PrizePlaceEntity extends AbstractEntity {

    @Column(name="place")
    private Integer place;

    @Column(name="prize")
    private Double prize;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="tourment_id")
    private TournamentEntity tournament;

}
