package kz.flowport.gameteam.dao.entities;

import kz.flowport.gameteam.dao.models.InvationAnswer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="invaites")
@Setter @Getter
public class InvatesEntity extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="squad_id")
    private SquadsEntity squad;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private UsersEntity user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="squad_capitan")
    private UsersEntity squadCapitan;

    @Enumerated(EnumType.ORDINAL)
    @Column(name="invation_answer")
    private InvationAnswer invationAnswer;


}
