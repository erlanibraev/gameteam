package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@MappedSuperclass
public abstract class AbstractLadder extends AbstractEntity {

    @Column(name="prize")
    @Getter @Setter
    private Double prizeFund;

    @Column(name="disqualification_cost")
    @Getter @Setter
    private Double disqualificationCost; // TODO: 03.05.2017 Уточнить
}
