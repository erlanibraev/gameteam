package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="general_monthly_ladder")
@Getter @Setter
public class GeneralMonthlyLadderEntity extends AbstractLadder {



    @Column(name="one_game_in_challenge_cost")
    private Double oneGameInChallengeCost; // TODO: 03.05.2017 Уточнить

    @Column(name="add_game_profile_cost")
    private Double addGameProfileCost; // TODO: 03.05.2017 Уточнить

    @Column(name="daily_enter_cost")
    private Double dailyEnterCost; // TODO: 03.05.2017 Уточнить

    @Column(name="share_sm_cost")
    private Double shareSmCost; // TODO: 03.05.2017 Уточнить

    @Column(name="join_to_challenge_cost")
    private Double joinToChallengeCost; // TODO: 03.05.2017 Уточнить

    @Column(name="leave_challenge_cost")
    private Double leaveChallengeCost; // TODO: 03.05.2017 Уточнить



}
