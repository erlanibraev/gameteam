package kz.flowport.gameteam.dao.entities;

import kz.flowport.gameteam.dao.models.Games;
import kz.flowport.gameteam.dao.models.TournamentGridType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="tournament_template")
@Setter @Getter
public class TournamentTemplateEntity extends AbstractEntity {

    @Enumerated(EnumType.ORDINAL)
    @Column(name="game_id")
    private Games gameId;

    @Column(name="tag", length = 1000)
    private String tag;

    @Column(name="description", length = 1000)
    private String description;

    @Column(name="mode", length = 1000)
    private String mode; // TODO: 03.05.2017 Уточнить!!!

    @Column(name="prize_fund")
    private Double prizeFund;

    @Column(name="prizePlace")
    private Integer prizePlace;

    @Column(name="comments", length = 1000)
    private String comments;

    @Column(name="gamemode", length = 1000)
    private String gamemode; // TODO: 03.05.2017 Уточнить!!!

    @Column(name="tournament_template_grid_type")
    @Enumerated(EnumType.ORDINAL)
    private TournamentGridType tournament_template_grid_type;

    @Column(name="deposit")
    private Double deposit;

}
