package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name = "squad_slot")
@Setter @Getter
public class SquadSlotEntity extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="slot")
    private UsersEntity slot;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="squad_id")
    private SquadsEntity squad;

}
