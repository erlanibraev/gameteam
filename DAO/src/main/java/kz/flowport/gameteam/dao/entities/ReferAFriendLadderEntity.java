package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="refer_a_friend_ladder")
@Getter @Setter
public class ReferAFriendLadderEntity extends AbstractEntity {
    @Column(name="refered_win_challenge_cost")
    private Double referedWinChallengeCost; // TODO: 03.05.2017 Уточнить

    @Column(name="refered_win_tournament_cost")
    private Double referedWinTournamentCost; // TODO: 03.05.2017 Уточнить

    @Column(name="refer_friend_cost")
    private Double referFriendCost; // TODO: 03.05.2017 Уточнить

}
