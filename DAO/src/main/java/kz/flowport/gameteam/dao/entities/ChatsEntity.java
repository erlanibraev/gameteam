package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="chats")
@Setter @Getter
public class ChatsEntity extends AbstractEntity {

    @Column(name="title")
    private String title;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<ChatUsersEntity> users;

    @Column(name="private_chat")
    private boolean privateChat = false;

}
