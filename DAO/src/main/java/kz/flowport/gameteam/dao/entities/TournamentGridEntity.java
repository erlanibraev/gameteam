package kz.flowport.gameteam.dao.entities;

import kz.flowport.gameteam.dao.models.TournamentGridType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="tournament_grid")
@Setter @Getter
public class TournamentGridEntity extends AbstractEntity {

    @Column(name="type")
    @Enumerated(EnumType.ORDINAL)
    private TournamentGridType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="tournament_id")
    private TournamentEntity tournament;

    @OneToMany(mappedBy = "tournamentGrid", fetch = FetchType.EAGER)
    private Set<RoundEntity> rounds;
}
