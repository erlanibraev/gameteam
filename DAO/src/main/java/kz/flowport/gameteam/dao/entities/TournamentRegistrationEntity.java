package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="tournament_registartion")
@Setter @Getter
public class TournamentRegistrationEntity extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tournament_id")
    private TournamentEntity tournament;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "team_id")
    private TeamsEntity team;
}
