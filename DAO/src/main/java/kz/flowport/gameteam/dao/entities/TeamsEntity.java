package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="teams", uniqueConstraints = {@UniqueConstraint(columnNames = {"team_name"}), @UniqueConstraint(columnNames = {"team_tag"})})
@Setter @Getter
public class TeamsEntity extends AbstractEntity {

    @Column(name="team_name", length = 1000)
    private String teamName;

    @Column(name="team_tag", length = 1000)
    private String teamTag;

    @Column(name="team_logo")
    @Lob
    private byte[] teamLogo;

    @Column(name="team_country", length = 2)
    private String teamCountry;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="chat_id")
    private ChatsEntity chat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="creator")
    private UsersEntity creator;

}
