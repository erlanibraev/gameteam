package kz.flowport.gameteam.dao.entities;

import kz.flowport.gameteam.dao.models.ChallengeTemplateType;
import kz.flowport.gameteam.dao.models.Games;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="challenge_template")
@Getter @Setter
public class ChallengeTemplateEntity extends AbstractEntity {

    @Column(name="game_id")
    @Enumerated(EnumType.ORDINAL)
    private Games game;

    @Column(name="challenge_template_name")
    private String challengeTemplateName;

    @Column(name="challenge_template_type")
    @Enumerated(EnumType.ORDINAL)
    private ChallengeTemplateType challengeTemplateType;

    @Column(name="min_mmr")
    private double min_mmr = 0;

    @Column(name="max_mmr")
    private double max_mmr = 0;

    @Column(name="deposit")
    private Double deposit;

}
