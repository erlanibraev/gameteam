package kz.flowport.gameteam.dao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="game_monthly_ladder")
public class GameMontlyLadderEntity extends AbstractLadder {

    @Column(name="private_challenge_win_cost")
    private Double privateChallengeWinCost; // TODO: 03.05.2017 Уточнить

    @Column(name="paid_challenge_win_cost")
    private Double paidChallengeWinCost; // TODO: 03.05.2017 Уточнить

    @Column(name="free_challenge_win_cost")
    private Double freeChallengeWinCost; // TODO: 03.05.2017 Уточнить

}
