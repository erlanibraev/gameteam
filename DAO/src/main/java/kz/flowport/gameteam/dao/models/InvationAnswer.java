package kz.flowport.gameteam.dao.models;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
public enum InvationAnswer {
    ACCEPT,
    DECLINE
}
