package kz.flowport.gameteam.dao.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Erlan Ibraev on 03.05.2017.
 */
@Entity
@Table(name="chat_users", uniqueConstraints = {@UniqueConstraint(columnNames={"user_id", "chat_id"})})
@Getter @Setter
public class ChatUsersEntity extends AbstractEntity{

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private UsersEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="chat_id")
    private ChatsEntity chat;

}
